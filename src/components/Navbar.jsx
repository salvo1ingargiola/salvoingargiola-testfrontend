/*
import logo from "../logo.png";

const navItems = [
  {name: "Home", path: "#home"},
  {name: "Band", path: "#band"},
  {name: "Tour", path: "#tour"},
  {name: "Contact", path: "#contact"},
  {name: "About us", path: "#more"},
  {name: "", path: "#search"}
];*/

export default function Navbar({logo, navItems}){
    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-secondary fixed-top" style={{opacity: 0.7}}>
            <div className="container-fluid">
                <a className="navbar-brand" href="#logo"><img src={logo} alt="Logo"></img></a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="navbar-nav">
                        {navItems.map((item, index) => (
                            <li key={index} className="nav-item">
                                {index === navItems.length-1 ? 
                                (<a className="nav-link" href={item.path}><i className="fas fa-search"></i></a>) :
                                (<a className="nav-link text-black" href={item.path}>
                                    {item.name}
                                </a>
                                )}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </nav>
    );
}