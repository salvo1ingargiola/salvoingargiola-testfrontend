/*import paris from '../paris.jpg';
import newYork from '../newyork.jpg';
import sanFrancisco from '../sanfrancisco.jpg';

const tourItems = [
     {name : "Paris", img: paris, text: "27 November 2015"},
     {name : "New York", img: newYork, text: "28 November 2015"},
     {name : "San Francisco", img: sanFrancisco, text: "29 November 2015"}
]

const dates = ["Semptember", "October", "November"];
*/

export default function Tour({dates, tourItems}){
    return(
        <section id="tour" style={{backgroundColor: "rgb(28, 28, 28)"}}>
            <h1 style={{letterSpacing: "5.5px", wordSpacing: "5.5px", textAlign: "center", lineHeight: "2.5em", color: "white"}}>TOUR DATES</h1>
            <p style={{color: "white", textAlign: "center", fontStyle: "italic"}}>Remember to book your tickets!<br/>Lorem ipsum we'll play you some music.</p>

            <section className="mb-3">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <table className="table text-white">
                                <tbody>
                                    {dates.map((date, index)=>(
                                        <tr key={index}>
                                            <td>{date}
                                            {index !== dates.length - 1 ? (
                                                <span style={{ color: "white", backgroundColor: "red", borderRadius: "50%", fontWeight: "bold", marginLeft: "10px", padding: "5px" }}>Sold Out!</span>
                                            ) : null}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            
            <div className="container">
                <div className="row justify-content-center">
                    {tourItems.map((tour, index) => (
                        <div key={index} className="col-md-4 mb-4">
                            <div className="card text-center">
                                <img src={tour.img} className="card-img-top" alt={tour.name} />
                                    <div className="card-body">
                                        <h5 className="card-title">{tour.name}</h5>
                                        <p className="card-text">{tour.text}</p>
                                        <a href="#tour" className="btn btn-dark">Buy Tickets</a>
                                    </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
}