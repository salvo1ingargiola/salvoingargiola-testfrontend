/*const contactInfo = [
  { icon: "bi bi-geo-alt-fill", text: "Chicago, US" },
  { icon: "bi bi-telephone-fill", text: "Phone: +00 1515151515" },
  { icon: "bi bi-envelope-fill", text: "Email: mail@mail.com" },
];

const formItems = [
    {type: "text", placeholder: "Name"},
    {type: "email", placeholder: "Email"}
];*/

export default function Contact({contactInfo, formItems}) {
    return (
        <section id="contact">
            <h1 style={{ letterSpacing: "5.5px", wordSpacing: "5.5px", textAlign: "center", lineHeight: "2.5em" }}>Contact</h1>
            <p style={{ textAlign: "center", fontStyle: "italic" }}>We love our fan!</p>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-5 text-end">
                        <p>Fan? Drop a note.</p>
                        {contactInfo.map((info, index) => (
                            <p key={index} style={{ marginBottom: "1rem" }}><i className={info.icon}></i> {info.text}</p>
                        ))}
                    </div>
                    <div className="col-md-5">
                        <form>
                            {formItems.map((item, index) => (
                                <div key={index} className="col-12" style={{ marginBottom: "1rem" }}>
                                    <input type={item.type} className="form-control" placeholder={item.placeholder} aria-label={item.placeholder} required />
                                </div>
                            ))}
                            <div className="col-12" style={{ marginBottom: "1rem" }}>
                                <textarea className="form-control" placeholder="Comment" aria-label="Comment" rows="3" required></textarea>
                            </div>
                            <div className="col-12 d-flex justify-content-end">
                                <button type="submit" className="btn btn-dark">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
}