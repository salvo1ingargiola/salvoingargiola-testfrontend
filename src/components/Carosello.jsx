/*import car1 from "../car1.avif";
import car2 from "../car2.jpg";
import car3 from "../car3.jpg";

const caroselloItems = [
    {source: "../car2.jpg", alt: "Immagine1"},
    {source: "../car3.jpg", alt: "Immagine2"},
    {source: "../car1.avif", alt: "Immagine3"}
]*/

export default function Navbar({caroselloItems}){
    return(
    <section id="home">
        <div id="carosello" className="carousel slide" data-ride="carousel">
            <div className="carousel-inner">
                {caroselloItems.map((car, index) => (
                    <div key={index} className={index === 0 ? "carousel-item active" : "carousel-item"}>
                    <img src={car.source} className="d-block w-100 img-fluid" alt={car.alt}/>
                    </div>
                ))}
            </div>


            <button className="carousel-control-prev" type="button" data-bs-target="#carosello" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carosello" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
            
        </div>
    </section>
    );
}