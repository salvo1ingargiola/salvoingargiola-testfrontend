/*
import singer from '../singer.webp';
import drummer from '../drummer.webp';
import guitarist from '../guitarist.jpg';

const bandItems = [
    {cardTitle: "Nome chitarrista", img: guitarist, alt: "Guitarist"},
    {cardTitle: "Nome cantante", img: singer, alt: "Singer"},
    {cardTitle: "Nome batterista", img: drummer, alt: "Drummer"}
]*/

export default function Band({bandItems}){
    return( 
        <section id="band">
                <div className="container">
                    <h1 style={{letterSpacing: "5.5px", wordSpacing: "5.5px", textAlign:"center", lineHeight: "2.5em"}}>THE BAND</h1>
                    <p style={{textAlign: "center", fontStyle: "italic"}}>We love music!</p>
                    <p style={{textAlign: "center", marginLeft: "15%", marginRight: "15%"}}>We have create a fictional band website.
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Alias suscipit temporibus sequi veniam praesentium.
                    Culpa officia laudantium quia molestias sed. Pariatur dolorum molestiae temporibus suscipit tempore praesentium 
                    ab nihil autem! Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusantium deserunt quasi 
                    voluptates nostrum similique odit modi. Corporis iure esse omnis enim ea, ipsum, voluptatum sapiente incidunt 
                    harum ad numquam? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Autem dolor explicabo, iusto 
                    asperiores ad odio nam eveniet nulla, sapiente sequi adipisci. Laudantium deleniti ipsum quis molestiae ullam 
                    aperiam asperiores impedit?</p>
                    
                    <div className="row">
                        {bandItems.map((band, index) => (
                            <div key={index} className="col-md-4 mb-4">
                                <div className="card border-0">
                                    <div className="card-body text-center">
                                        <h5 className="card-title">{band.cardTitle}</h5>
                                        <img src={band.img} class="card-img-top mx-auto" alt={band.alt}></img>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
        </section>
    );
}