/*const footerItems = [
  { name: "Home", path: "#home" },
  { name: "Band", path: "#band" },
  { name: "Tour", path: "#tour" },
  { name: "Contact", path: "#contact" },
];

const contactInfo = [
  { icon: "bi bi-geo-alt-fill", text: "Chicago, US" },
  { icon: "bi bi-telephone-fill", text: "Phone: +00 1515151515" },
  { icon: "bi bi-envelope-fill", text: "Email: mail@mail.com" },
];

const links = [
  {link: "https://twitter.com/twipsum", textClass: "twitter"},
  {link: "https://www.facebook.com/loremipsumwebsite", textClass: "facebook"},
  {link: "https://www.instagram.com/explore/tags/loremispum", textClass: "instagram"},
  {link: "https://youtu.be/BggrpKfqh1c?si=nrc8-WPoCYRQXUXQ", textClass: "youtube"},

];*/

export default function Footer({footerItems, contactInfo, links}) {
  return (
    <footer id="more" className="bg-secondary text-white pt-4 pb-2 mt-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <h4 className="mb-4">About us:</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
          <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <h5 className="mb-4">Links</h5>
            <ul className="list-unstyled">
              {footerItems.map((item, index) => (
                <li key={index} className="mb-2"><a href={item.path} className="text-white">{item.name}</a></li>
              ))}
            </ul>
          </div>
          <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <h5 className="mb-4">CONTACT</h5>
            {contactInfo.map((info, index) => (
              <p key={index}><i className={info.icon}></i> {info.text}</p>
            ))}
            <h5 className="mt-4 mb-4">FOLLOW US</h5>
            <div>
              {links.map((info, index)=> (
                <a key={index} href={info.link} className="text-white pe-2" target="_blank" rel="noopener noreferrer">
                  <i className={`bi bi-${info.textClass}`}></i>
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}