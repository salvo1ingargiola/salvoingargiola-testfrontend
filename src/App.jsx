import './App.css'

import Navbar from './components/Navbar'
import logo from './components/logo.png'

import Carosello from './components/Carosello'
import car1 from './components/car1.avif'
import car2 from './components/car2.jpg'
import car3 from './components/car3.jpg'

import Band from './components/Band'
import singer from './components/singer.webp'
import drummer from './components/drummer.webp'
import guitarist from './components/guitarist.jpg'

import Tour from './components/Tour'
import paris from './components/paris.jpg';
import newYork from './components/newyork.jpg';
import sanFrancisco from './components/sanfrancisco.jpg';

import Contact from './components/Contact'

import Footer from './components/Footer'

const navItems = [
  {name: "Home", path: "#home"},
  {name: "Band", path: "#band"},
  {name: "Tour", path: "#tour"},
  {name: "Contact", path: "#contact"},
  {name: "About us", path: "#more"},
  {name: "", path: "#search"}
];

const caroselloItems = [
    {source: car2, alt: "Immagine1"},
    {source: car3, alt: "Immagine2"},
    {source: car1, alt: "Immagine3"}
];

const bandItems = [
    {cardTitle: "Nome chitarrista", img: singer, alt: "Guitarist"},
    {cardTitle: "Nome cantante", img: drummer, alt: "Singer"},
    {cardTitle: "Nome batterista", img: guitarist, alt: "Drummer"}
];

const tourItems = [
     {name : "Paris", img: paris, text: "27 November 2015"},
     {name : "New York", img: newYork, text: "28 November 2015"},
     {name : "San Francisco", img: sanFrancisco, text: "29 November 2015"}
];

const dates = ["Semptember", "October", "November"];

const contactInfo = [
  { icon: "bi bi-geo-alt-fill", text: "Chicago, US" },
  { icon: "bi bi-telephone-fill", text: "Phone: +00 1515151515" },
  { icon: "bi bi-envelope-fill", text: "Email: mail@mail.com" },
];

const formItems = [
    {type: "text", placeholder: "Name"},
    {type: "email", placeholder: "Email"}
];

const links = [
  {link: "https://twitter.com/twipsum", textClass: "twitter"},
  {link: "https://www.facebook.com/loremipsumwebsite", textClass: "facebook"},
  {link: "https://www.instagram.com/explore/tags/loremispum", textClass: "instagram"},
  {link: "https://youtu.be/BggrpKfqh1c?si=nrc8-WPoCYRQXUXQ", textClass: "youtube"},

];

function App() {
  return (
    <div>
      <Navbar logo={logo} navItems={navItems}/>
      <Carosello caroselloItems={caroselloItems}/>
      <Band bandItems={bandItems}/>
      <Tour dates={dates} tourItems={tourItems}/>
      <Contact contactInfo={contactInfo} formItems={formItems}/>
      <Footer footerItems={navItems} contactInfo={contactInfo} links={links}/>
    </div>
  )
}

export default App
